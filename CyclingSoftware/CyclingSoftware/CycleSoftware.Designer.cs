﻿namespace CyclingSoftware
{
    partial class CycleSoftware
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange11 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange12 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange13 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange14 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange15 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            this.topNavPanel = new System.Windows.Forms.Panel();
            this.workoutSwitchToCalendarButton = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.workoutSwitchToGridButton = new System.Windows.Forms.Button();
            this.importWorkoutsButton = new System.Windows.Forms.Button();
            this.UnitsComboBox = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.FTPLabel = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mainContentPanel = new System.Windows.Forms.Panel();
            this.workoutSessionsCalendarPanel = new System.Windows.Forms.Panel();
            this.workoutSessionPanel = new System.Windows.Forms.Panel();
            this.workoutSessionTableViewPanel = new System.Windows.Forms.Panel();
            this.workoutSessionMaxAltitude = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.workoutSessionAverageAltitude = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.workoutSessionMaxPower = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.workoutSessionAveragePower = new System.Windows.Forms.Label();
            this.workoutSessionMaxHeartRate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.workoutSessionAverageHeartRate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.workoutSessionMaximumSpeed = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.workoutSessionAverageSpeed = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.workoutSessionTotalDistanceCovered = new System.Windows.Forms.Label();
            this.distanceLabel = new System.Windows.Forms.Label();
            this.workoutSessionTableGridView = new System.Windows.Forms.DataGridView();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workoutSessionLengthLabel = new System.Windows.Forms.Label();
            this.workoutSessionIntervalLabel = new System.Windows.Forms.Label();
            this.workoutSessionDateAndTime = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tableViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workoutSessionIntervalPanel = new System.Windows.Forms.Panel();
            this.workoutSessionGraphViewPanel = new System.Windows.Forms.Panel();
            this.workoutSessionGraphMaxAltitude = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.workoutSessionGraphAverageAltitude = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.workoutSessionGraphMaxPower = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.workoutSessionGraphAveragePower = new System.Windows.Forms.Label();
            this.workoutSessionGraphMaxHeartRate = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.workoutSessionGraphAverageHeartRate = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.workoutSessionGraphMaximumSpeed = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.workoutSessionGraphAverageSpeed = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.workoutSessionGraphTotalDistanceCovered = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.workoutSessionGraphRangeLabel = new System.Windows.Forms.Label();
            this.workoutSessionUpdateShowOriginalData = new System.Windows.Forms.Button();
            this.workoutSessionUpdateDataRangeButton = new System.Windows.Forms.Button();
            this.workoutSessionTrackBarFinish = new System.Windows.Forms.TrackBar();
            this.workoutSessionTrackBarStart = new System.Windows.Forms.TrackBar();
            this.workoutSessionGraphNP = new System.Windows.Forms.Label();
            this.workoutSessionGraphIF = new System.Windows.Forms.Label();
            this.workoutSessionGraphTSS = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.workoutSessionGraphFilter = new System.Windows.Forms.CheckedListBox();
            this.workoutSessionGraph = new ZedGraph.ZedGraphControl();
            this.workoutSessionsPanel = new System.Windows.Forms.Panel();
            this.workoutSessionsGridView = new System.Windows.Forms.DataGridView();
            this.openWorkoutDialog = new System.Windows.Forms.OpenFileDialog();
            this.openWorkoutsDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.workoutSessionsCalendar = new System.Windows.Forms.Calendar.Calendar();
            this.workoutSessionsCalendarMonth = new System.Windows.Forms.Calendar.MonthView();
            this.effg = new System.Windows.Forms.Label();
            this.workoutSessionTSS = new System.Windows.Forms.Label();
            this.workoutSessionNP = new System.Windows.Forms.Label();
            this.workoutSessionIF = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.workoutSessionGraphMinHeartRate = new System.Windows.Forms.Label();
            this.workoutSessionMinHeartRate = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.importWorkoutButton = new System.Windows.Forms.Button();
            this.WorkoutID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkoutTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkoutDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkoutInterval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkoutLinkButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.topNavPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.mainContentPanel.SuspendLayout();
            this.workoutSessionsCalendarPanel.SuspendLayout();
            this.workoutSessionPanel.SuspendLayout();
            this.workoutSessionTableViewPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workoutSessionTableGridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.workoutSessionGraphViewPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workoutSessionTrackBarFinish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workoutSessionTrackBarStart)).BeginInit();
            this.workoutSessionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workoutSessionsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // topNavPanel
            // 
            this.topNavPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(170)))), ((int)(((byte)(245)))));
            this.topNavPanel.Controls.Add(this.importWorkoutButton);
            this.topNavPanel.Controls.Add(this.workoutSwitchToCalendarButton);
            this.topNavPanel.Controls.Add(this.label31);
            this.topNavPanel.Controls.Add(this.workoutSwitchToGridButton);
            this.topNavPanel.Controls.Add(this.importWorkoutsButton);
            this.topNavPanel.Controls.Add(this.UnitsComboBox);
            this.topNavPanel.Controls.Add(this.label30);
            this.topNavPanel.Controls.Add(this.FTPLabel);
            this.topNavPanel.Controls.Add(this.label29);
            this.topNavPanel.Controls.Add(this.pictureBox1);
            this.topNavPanel.Location = new System.Drawing.Point(0, 0);
            this.topNavPanel.Name = "topNavPanel";
            this.topNavPanel.Size = new System.Drawing.Size(1042, 106);
            this.topNavPanel.TabIndex = 0;
            // 
            // workoutSwitchToCalendarButton
            // 
            this.workoutSwitchToCalendarButton.BackColor = System.Drawing.SystemColors.HotTrack;
            this.workoutSwitchToCalendarButton.FlatAppearance.BorderSize = 0;
            this.workoutSwitchToCalendarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.workoutSwitchToCalendarButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSwitchToCalendarButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.workoutSwitchToCalendarButton.Location = new System.Drawing.Point(684, 55);
            this.workoutSwitchToCalendarButton.Name = "workoutSwitchToCalendarButton";
            this.workoutSwitchToCalendarButton.Size = new System.Drawing.Size(97, 25);
            this.workoutSwitchToCalendarButton.TabIndex = 10;
            this.workoutSwitchToCalendarButton.Text = "Calendar";
            this.workoutSwitchToCalendarButton.UseVisualStyleBackColor = false;
            this.workoutSwitchToCalendarButton.Click += new System.EventHandler(this.workoutSwitchToCalendarButton_Click);
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.CornflowerBlue;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.GhostWhite;
            this.label31.Location = new System.Drawing.Point(585, 24);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(196, 29);
            this.label31.TabIndex = 9;
            this.label31.Text = "View Workouts";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // workoutSwitchToGridButton
            // 
            this.workoutSwitchToGridButton.BackColor = System.Drawing.SystemColors.HotTrack;
            this.workoutSwitchToGridButton.FlatAppearance.BorderSize = 0;
            this.workoutSwitchToGridButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.workoutSwitchToGridButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSwitchToGridButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.workoutSwitchToGridButton.Location = new System.Drawing.Point(585, 55);
            this.workoutSwitchToGridButton.Name = "workoutSwitchToGridButton";
            this.workoutSwitchToGridButton.Size = new System.Drawing.Size(97, 25);
            this.workoutSwitchToGridButton.TabIndex = 8;
            this.workoutSwitchToGridButton.Text = "Grid";
            this.workoutSwitchToGridButton.UseVisualStyleBackColor = false;
            this.workoutSwitchToGridButton.Click += new System.EventHandler(this.workoutSwitchToGridButton_Click);
            // 
            // importWorkoutsButton
            // 
            this.importWorkoutsButton.BackColor = System.Drawing.SystemColors.HotTrack;
            this.importWorkoutsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.importWorkoutsButton.FlatAppearance.BorderSize = 0;
            this.importWorkoutsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.importWorkoutsButton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importWorkoutsButton.ForeColor = System.Drawing.Color.Snow;
            this.importWorkoutsButton.Location = new System.Drawing.Point(282, 33);
            this.importWorkoutsButton.Margin = new System.Windows.Forms.Padding(0);
            this.importWorkoutsButton.Name = "importWorkoutsButton";
            this.importWorkoutsButton.Size = new System.Drawing.Size(126, 42);
            this.importWorkoutsButton.TabIndex = 6;
            this.importWorkoutsButton.Text = "Import Folder";
            this.importWorkoutsButton.UseVisualStyleBackColor = false;
            this.importWorkoutsButton.Click += new System.EventHandler(this.importWorkoutsButton_Click);
            // 
            // UnitsComboBox
            // 
            this.UnitsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UnitsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UnitsComboBox.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnitsComboBox.FormattingEnabled = true;
            this.UnitsComboBox.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.UnitsComboBox.Location = new System.Drawing.Point(823, 51);
            this.UnitsComboBox.Name = "UnitsComboBox";
            this.UnitsComboBox.Size = new System.Drawing.Size(92, 21);
            this.UnitsComboBox.TabIndex = 5;
            this.UnitsComboBox.TabStop = false;
            this.UnitsComboBox.SelectionChangeCommitted += new System.EventHandler(this.UnitsComboBox_SelectionChangeCommitted);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label30.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label30.Location = new System.Drawing.Point(842, 21);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 25);
            this.label30.TabIndex = 4;
            this.label30.Text = "Units";
            // 
            // FTPLabel
            // 
            this.FTPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FTPLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.FTPLabel.Location = new System.Drawing.Point(927, 48);
            this.FTPLabel.Name = "FTPLabel";
            this.FTPLabel.Size = new System.Drawing.Size(100, 23);
            this.FTPLabel.TabIndex = 3;
            this.FTPLabel.Text = "label30";
            this.FTPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label29.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label29.Location = new System.Drawing.Point(934, 21);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(85, 25);
            this.label29.TabIndex = 2;
            this.label29.Text = "Your FTP";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CyclingSoftware.Properties.Resources.cyclesoftware;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(252, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // mainContentPanel
            // 
            this.mainContentPanel.BackColor = System.Drawing.SystemColors.HighlightText;
            this.mainContentPanel.Controls.Add(this.workoutSessionPanel);
            this.mainContentPanel.Controls.Add(this.workoutSessionsCalendarPanel);
            this.mainContentPanel.Controls.Add(this.workoutSessionsPanel);
            this.mainContentPanel.Location = new System.Drawing.Point(0, 103);
            this.mainContentPanel.Name = "mainContentPanel";
            this.mainContentPanel.Size = new System.Drawing.Size(1042, 540);
            this.mainContentPanel.TabIndex = 1;
            // 
            // workoutSessionsCalendarPanel
            // 
            this.workoutSessionsCalendarPanel.Controls.Add(this.workoutSessionsCalendarMonth);
            this.workoutSessionsCalendarPanel.Controls.Add(this.workoutSessionsCalendar);
            this.workoutSessionsCalendarPanel.Location = new System.Drawing.Point(1, 1);
            this.workoutSessionsCalendarPanel.Name = "workoutSessionsCalendarPanel";
            this.workoutSessionsCalendarPanel.Size = new System.Drawing.Size(1044, 540);
            this.workoutSessionsCalendarPanel.TabIndex = 2;
            // 
            // workoutSessionPanel
            // 
            this.workoutSessionPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.workoutSessionPanel.Controls.Add(this.workoutSessionLengthLabel);
            this.workoutSessionPanel.Controls.Add(this.workoutSessionIntervalLabel);
            this.workoutSessionPanel.Controls.Add(this.workoutSessionDateAndTime);
            this.workoutSessionPanel.Controls.Add(this.menuStrip1);
            this.workoutSessionPanel.Controls.Add(this.workoutSessionIntervalPanel);
            this.workoutSessionPanel.Controls.Add(this.workoutSessionGraphViewPanel);
            this.workoutSessionPanel.Controls.Add(this.workoutSessionTableViewPanel);
            this.workoutSessionPanel.Location = new System.Drawing.Point(0, 0);
            this.workoutSessionPanel.Name = "workoutSessionPanel";
            this.workoutSessionPanel.Size = new System.Drawing.Size(1042, 537);
            this.workoutSessionPanel.TabIndex = 0;
            // 
            // workoutSessionTableViewPanel
            // 
            this.workoutSessionTableViewPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionMinHeartRate);
            this.workoutSessionTableViewPanel.Controls.Add(this.label15);
            this.workoutSessionTableViewPanel.Controls.Add(this.label33);
            this.workoutSessionTableViewPanel.Controls.Add(this.label32);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionIF);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionNP);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionTSS);
            this.workoutSessionTableViewPanel.Controls.Add(this.effg);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionMaxAltitude);
            this.workoutSessionTableViewPanel.Controls.Add(this.label9);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionAverageAltitude);
            this.workoutSessionTableViewPanel.Controls.Add(this.label7);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionMaxPower);
            this.workoutSessionTableViewPanel.Controls.Add(this.label6);
            this.workoutSessionTableViewPanel.Controls.Add(this.label5);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionAveragePower);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionMaxHeartRate);
            this.workoutSessionTableViewPanel.Controls.Add(this.label4);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionAverageHeartRate);
            this.workoutSessionTableViewPanel.Controls.Add(this.label3);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionMaximumSpeed);
            this.workoutSessionTableViewPanel.Controls.Add(this.label);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionAverageSpeed);
            this.workoutSessionTableViewPanel.Controls.Add(this.label2);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionTotalDistanceCovered);
            this.workoutSessionTableViewPanel.Controls.Add(this.distanceLabel);
            this.workoutSessionTableViewPanel.Controls.Add(this.workoutSessionTableGridView);
            this.workoutSessionTableViewPanel.Location = new System.Drawing.Point(1, 95);
            this.workoutSessionTableViewPanel.Name = "workoutSessionTableViewPanel";
            this.workoutSessionTableViewPanel.Size = new System.Drawing.Size(1042, 441);
            this.workoutSessionTableViewPanel.TabIndex = 2;
            // 
            // workoutSessionMaxAltitude
            // 
            this.workoutSessionMaxAltitude.AutoSize = true;
            this.workoutSessionMaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionMaxAltitude.Location = new System.Drawing.Point(810, 267);
            this.workoutSessionMaxAltitude.Name = "workoutSessionMaxAltitude";
            this.workoutSessionMaxAltitude.Size = new System.Drawing.Size(44, 17);
            this.workoutSessionMaxAltitude.TabIndex = 18;
            this.workoutSessionMaxAltitude.Text = "3000ft";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(769, 234);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 19);
            this.label9.TabIndex = 17;
            this.label9.Text = "Maximum Altitude";
            // 
            // workoutSessionAverageAltitude
            // 
            this.workoutSessionAverageAltitude.AutoSize = true;
            this.workoutSessionAverageAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionAverageAltitude.Location = new System.Drawing.Point(651, 267);
            this.workoutSessionAverageAltitude.Name = "workoutSessionAverageAltitude";
            this.workoutSessionAverageAltitude.Size = new System.Drawing.Size(37, 17);
            this.workoutSessionAverageAltitude.TabIndex = 16;
            this.workoutSessionAverageAltitude.Text = "300ft";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(613, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 19);
            this.label7.TabIndex = 15;
            this.label7.Text = "Average Altitude";
            // 
            // workoutSessionMaxPower
            // 
            this.workoutSessionMaxPower.AutoSize = true;
            this.workoutSessionMaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionMaxPower.Location = new System.Drawing.Point(802, 197);
            this.workoutSessionMaxPower.Name = "workoutSessionMaxPower";
            this.workoutSessionMaxPower.Size = new System.Drawing.Size(63, 17);
            this.workoutSessionMaxPower.TabIndex = 14;
            this.workoutSessionMaxPower.Text = "800 watts";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(778, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "Maximum Power";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(624, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "Average Power";
            // 
            // workoutSessionAveragePower
            // 
            this.workoutSessionAveragePower.AutoSize = true;
            this.workoutSessionAveragePower.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionAveragePower.Location = new System.Drawing.Point(640, 197);
            this.workoutSessionAveragePower.Name = "workoutSessionAveragePower";
            this.workoutSessionAveragePower.Size = new System.Drawing.Size(63, 17);
            this.workoutSessionAveragePower.TabIndex = 11;
            this.workoutSessionAveragePower.Text = "420 watts";
            // 
            // workoutSessionMaxHeartRate
            // 
            this.workoutSessionMaxHeartRate.AutoSize = true;
            this.workoutSessionMaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionMaxHeartRate.Location = new System.Drawing.Point(802, 126);
            this.workoutSessionMaxHeartRate.Name = "workoutSessionMaxHeartRate";
            this.workoutSessionMaxHeartRate.Size = new System.Drawing.Size(56, 17);
            this.workoutSessionMaxHeartRate.TabIndex = 10;
            this.workoutSessionMaxHeartRate.Text = "130bpm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(764, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 19);
            this.label4.TabIndex = 9;
            this.label4.Text = "Maximum Heart Rate";
            // 
            // workoutSessionAverageHeartRate
            // 
            this.workoutSessionAverageHeartRate.AutoSize = true;
            this.workoutSessionAverageHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionAverageHeartRate.Location = new System.Drawing.Point(651, 126);
            this.workoutSessionAverageHeartRate.Name = "workoutSessionAverageHeartRate";
            this.workoutSessionAverageHeartRate.Size = new System.Drawing.Size(48, 17);
            this.workoutSessionAverageHeartRate.TabIndex = 8;
            this.workoutSessionAverageHeartRate.Text = "12mph";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(607, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Average Heart Rate";
            // 
            // workoutSessionMaximumSpeed
            // 
            this.workoutSessionMaximumSpeed.AutoSize = true;
            this.workoutSessionMaximumSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionMaximumSpeed.Location = new System.Drawing.Point(810, 56);
            this.workoutSessionMaximumSpeed.Name = "workoutSessionMaximumSpeed";
            this.workoutSessionMaximumSpeed.Size = new System.Drawing.Size(48, 17);
            this.workoutSessionMaximumSpeed.TabIndex = 6;
            this.workoutSessionMaximumSpeed.Text = "40mph";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label.Location = new System.Drawing.Point(779, 28);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(114, 19);
            this.label.TabIndex = 5;
            this.label.Text = "Maximum Speed";
            // 
            // workoutSessionAverageSpeed
            // 
            this.workoutSessionAverageSpeed.AutoSize = true;
            this.workoutSessionAverageSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionAverageSpeed.Location = new System.Drawing.Point(651, 56);
            this.workoutSessionAverageSpeed.Name = "workoutSessionAverageSpeed";
            this.workoutSessionAverageSpeed.Size = new System.Drawing.Size(48, 17);
            this.workoutSessionAverageSpeed.TabIndex = 4;
            this.workoutSessionAverageSpeed.Text = "34mph";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(624, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Average Speed";
            // 
            // workoutSessionTotalDistanceCovered
            // 
            this.workoutSessionTotalDistanceCovered.AutoSize = true;
            this.workoutSessionTotalDistanceCovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionTotalDistanceCovered.Location = new System.Drawing.Point(642, 333);
            this.workoutSessionTotalDistanceCovered.Name = "workoutSessionTotalDistanceCovered";
            this.workoutSessionTotalDistanceCovered.Size = new System.Drawing.Size(46, 17);
            this.workoutSessionTotalDistanceCovered.TabIndex = 2;
            this.workoutSessionTotalDistanceCovered.Text = "300km";
            // 
            // distanceLabel
            // 
            this.distanceLabel.AutoSize = true;
            this.distanceLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.distanceLabel.Location = new System.Drawing.Point(593, 302);
            this.distanceLabel.Name = "distanceLabel";
            this.distanceLabel.Size = new System.Drawing.Size(153, 19);
            this.distanceLabel.TabIndex = 1;
            this.distanceLabel.Text = "Total Distance Covered";
            // 
            // workoutSessionTableGridView
            // 
            this.workoutSessionTableGridView.AllowUserToAddRows = false;
            this.workoutSessionTableGridView.AllowUserToDeleteRows = false;
            this.workoutSessionTableGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.workoutSessionTableGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HeartRate,
            this.Speed,
            this.Cadence,
            this.Altitude,
            this.Power});
            this.workoutSessionTableGridView.Location = new System.Drawing.Point(26, 27);
            this.workoutSessionTableGridView.Name = "workoutSessionTableGridView";
            this.workoutSessionTableGridView.ReadOnly = true;
            this.workoutSessionTableGridView.RowHeadersVisible = false;
            this.workoutSessionTableGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.workoutSessionTableGridView.Size = new System.Drawing.Size(504, 398);
            this.workoutSessionTableGridView.TabIndex = 0;
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "Heart Rate";
            this.HeartRate.Name = "HeartRate";
            this.HeartRate.ReadOnly = true;
            // 
            // Speed
            // 
            this.Speed.HeaderText = "Speed";
            this.Speed.Name = "Speed";
            this.Speed.ReadOnly = true;
            // 
            // Cadence
            // 
            this.Cadence.HeaderText = "Cadence";
            this.Cadence.Name = "Cadence";
            this.Cadence.ReadOnly = true;
            // 
            // Altitude
            // 
            this.Altitude.HeaderText = "Altitude";
            this.Altitude.Name = "Altitude";
            this.Altitude.ReadOnly = true;
            // 
            // Power
            // 
            this.Power.HeaderText = "Power";
            this.Power.Name = "Power";
            this.Power.ReadOnly = true;
            // 
            // workoutSessionLengthLabel
            // 
            this.workoutSessionLengthLabel.AutoSize = true;
            this.workoutSessionLengthLabel.Location = new System.Drawing.Point(470, 40);
            this.workoutSessionLengthLabel.Name = "workoutSessionLengthLabel";
            this.workoutSessionLengthLabel.Size = new System.Drawing.Size(73, 13);
            this.workoutSessionLengthLabel.TabIndex = 6;
            this.workoutSessionLengthLabel.Text = "Length: 38:20";
            // 
            // workoutSessionIntervalLabel
            // 
            this.workoutSessionIntervalLabel.AutoSize = true;
            this.workoutSessionIntervalLabel.Location = new System.Drawing.Point(575, 40);
            this.workoutSessionIntervalLabel.Name = "workoutSessionIntervalLabel";
            this.workoutSessionIntervalLabel.Size = new System.Drawing.Size(94, 13);
            this.workoutSessionIntervalLabel.TabIndex = 5;
            this.workoutSessionIntervalLabel.Text = "Interval: 1 Second";
            // 
            // workoutSessionDateAndTime
            // 
            this.workoutSessionDateAndTime.AutoSize = true;
            this.workoutSessionDateAndTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.workoutSessionDateAndTime.ForeColor = System.Drawing.SystemColors.WindowText;
            this.workoutSessionDateAndTime.Location = new System.Drawing.Point(21, 32);
            this.workoutSessionDateAndTime.Name = "workoutSessionDateAndTime";
            this.workoutSessionDateAndTime.Size = new System.Drawing.Size(426, 24);
            this.workoutSessionDateAndTime.TabIndex = 0;
            this.workoutSessionDateAndTime.Text = "Workout On: 12th September 2015 at 3:59pm";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tableViewToolStripMenuItem,
            this.graphViewToolStripMenuItem,
            this.intervalToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(776, 39);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(339, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tableViewToolStripMenuItem
            // 
            this.tableViewToolStripMenuItem.Name = "tableViewToolStripMenuItem";
            this.tableViewToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.tableViewToolStripMenuItem.Text = "Table View";
            this.tableViewToolStripMenuItem.Click += new System.EventHandler(this.tableViewToolStripMenuItem_Click);
            // 
            // graphViewToolStripMenuItem
            // 
            this.graphViewToolStripMenuItem.Name = "graphViewToolStripMenuItem";
            this.graphViewToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.graphViewToolStripMenuItem.Text = "Graph View";
            this.graphViewToolStripMenuItem.Click += new System.EventHandler(this.graphViewToolStripMenuItem_Click);
            // 
            // intervalToolStripMenuItem
            // 
            this.intervalToolStripMenuItem.Name = "intervalToolStripMenuItem";
            this.intervalToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.intervalToolStripMenuItem.Text = "Interval Data";
            this.intervalToolStripMenuItem.Click += new System.EventHandler(this.intervalToolStripMenuItem_Click);
            // 
            // workoutSessionIntervalPanel
            // 
            this.workoutSessionIntervalPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.workoutSessionIntervalPanel.Location = new System.Drawing.Point(2, 95);
            this.workoutSessionIntervalPanel.Name = "workoutSessionIntervalPanel";
            this.workoutSessionIntervalPanel.Size = new System.Drawing.Size(1041, 446);
            this.workoutSessionIntervalPanel.TabIndex = 4;
            // 
            // workoutSessionGraphViewPanel
            // 
            this.workoutSessionGraphViewPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphMinHeartRate);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label11);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphMaxAltitude);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label12);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphAverageAltitude);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label14);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphMaxPower);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label16);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label17);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphAveragePower);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphMaxHeartRate);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label20);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphAverageHeartRate);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label22);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphMaximumSpeed);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label24);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphAverageSpeed);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label26);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphTotalDistanceCovered);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label28);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphRangeLabel);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionUpdateShowOriginalData);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionUpdateDataRangeButton);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionTrackBarFinish);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionTrackBarStart);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphNP);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphIF);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphTSS);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label10);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label8);
            this.workoutSessionGraphViewPanel.Controls.Add(this.label1);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraphFilter);
            this.workoutSessionGraphViewPanel.Controls.Add(this.workoutSessionGraph);
            this.workoutSessionGraphViewPanel.Location = new System.Drawing.Point(1, 94);
            this.workoutSessionGraphViewPanel.Name = "workoutSessionGraphViewPanel";
            this.workoutSessionGraphViewPanel.Size = new System.Drawing.Size(1042, 443);
            this.workoutSessionGraphViewPanel.TabIndex = 3;
            // 
            // workoutSessionGraphMaxAltitude
            // 
            this.workoutSessionGraphMaxAltitude.AutoSize = true;
            this.workoutSessionGraphMaxAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphMaxAltitude.Location = new System.Drawing.Point(870, 268);
            this.workoutSessionGraphMaxAltitude.Name = "workoutSessionGraphMaxAltitude";
            this.workoutSessionGraphMaxAltitude.Size = new System.Drawing.Size(44, 17);
            this.workoutSessionGraphMaxAltitude.TabIndex = 36;
            this.workoutSessionGraphMaxAltitude.Text = "3000ft";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(830, 238);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 19);
            this.label12.TabIndex = 35;
            this.label12.Text = "Maximum Altitude";
            // 
            // workoutSessionGraphAverageAltitude
            // 
            this.workoutSessionGraphAverageAltitude.AutoSize = true;
            this.workoutSessionGraphAverageAltitude.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphAverageAltitude.Location = new System.Drawing.Point(708, 268);
            this.workoutSessionGraphAverageAltitude.Name = "workoutSessionGraphAverageAltitude";
            this.workoutSessionGraphAverageAltitude.Size = new System.Drawing.Size(37, 17);
            this.workoutSessionGraphAverageAltitude.TabIndex = 34;
            this.workoutSessionGraphAverageAltitude.Text = "300ft";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(666, 235);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(113, 19);
            this.label14.TabIndex = 33;
            this.label14.Text = "Average Altitude";
            // 
            // workoutSessionGraphMaxPower
            // 
            this.workoutSessionGraphMaxPower.AutoSize = true;
            this.workoutSessionGraphMaxPower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphMaxPower.Location = new System.Drawing.Point(852, 198);
            this.workoutSessionGraphMaxPower.Name = "workoutSessionGraphMaxPower";
            this.workoutSessionGraphMaxPower.Size = new System.Drawing.Size(63, 17);
            this.workoutSessionGraphMaxPower.TabIndex = 32;
            this.workoutSessionGraphMaxPower.Text = "800 watts";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(831, 164);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 19);
            this.label16.TabIndex = 31;
            this.label16.Text = "Maximum Power";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(672, 164);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 19);
            this.label17.TabIndex = 30;
            this.label17.Text = "Average Power";
            // 
            // workoutSessionGraphAveragePower
            // 
            this.workoutSessionGraphAveragePower.AutoSize = true;
            this.workoutSessionGraphAveragePower.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphAveragePower.Location = new System.Drawing.Point(692, 198);
            this.workoutSessionGraphAveragePower.Name = "workoutSessionGraphAveragePower";
            this.workoutSessionGraphAveragePower.Size = new System.Drawing.Size(63, 17);
            this.workoutSessionGraphAveragePower.TabIndex = 29;
            this.workoutSessionGraphAveragePower.Text = "420 watts";
            // 
            // workoutSessionGraphMaxHeartRate
            // 
            this.workoutSessionGraphMaxHeartRate.AutoSize = true;
            this.workoutSessionGraphMaxHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphMaxHeartRate.Location = new System.Drawing.Point(807, 127);
            this.workoutSessionGraphMaxHeartRate.Name = "workoutSessionGraphMaxHeartRate";
            this.workoutSessionGraphMaxHeartRate.Size = new System.Drawing.Size(56, 17);
            this.workoutSessionGraphMaxHeartRate.TabIndex = 28;
            this.workoutSessionGraphMaxHeartRate.Text = "130bpm";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(765, 95);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(142, 19);
            this.label20.TabIndex = 27;
            this.label20.Text = "Maximum Heart Rate";
            // 
            // workoutSessionGraphAverageHeartRate
            // 
            this.workoutSessionGraphAverageHeartRate.AutoSize = true;
            this.workoutSessionGraphAverageHeartRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphAverageHeartRate.Location = new System.Drawing.Point(658, 127);
            this.workoutSessionGraphAverageHeartRate.Name = "workoutSessionGraphAverageHeartRate";
            this.workoutSessionGraphAverageHeartRate.Size = new System.Drawing.Size(48, 17);
            this.workoutSessionGraphAverageHeartRate.TabIndex = 26;
            this.workoutSessionGraphAverageHeartRate.Text = "12mph";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(630, 95);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(129, 19);
            this.label22.TabIndex = 25;
            this.label22.Text = "Average Heart Rate";
            // 
            // workoutSessionGraphMaximumSpeed
            // 
            this.workoutSessionGraphMaximumSpeed.AutoSize = true;
            this.workoutSessionGraphMaximumSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphMaximumSpeed.Location = new System.Drawing.Point(867, 57);
            this.workoutSessionGraphMaximumSpeed.Name = "workoutSessionGraphMaximumSpeed";
            this.workoutSessionGraphMaximumSpeed.Size = new System.Drawing.Size(48, 17);
            this.workoutSessionGraphMaximumSpeed.TabIndex = 24;
            this.workoutSessionGraphMaximumSpeed.Text = "40mph";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(830, 29);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(114, 19);
            this.label24.TabIndex = 23;
            this.label24.Text = "Maximum Speed";
            // 
            // workoutSessionGraphAverageSpeed
            // 
            this.workoutSessionGraphAverageSpeed.AutoSize = true;
            this.workoutSessionGraphAverageSpeed.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphAverageSpeed.Location = new System.Drawing.Point(697, 57);
            this.workoutSessionGraphAverageSpeed.Name = "workoutSessionGraphAverageSpeed";
            this.workoutSessionGraphAverageSpeed.Size = new System.Drawing.Size(48, 17);
            this.workoutSessionGraphAverageSpeed.TabIndex = 22;
            this.workoutSessionGraphAverageSpeed.Text = "34mph";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(666, 29);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(101, 19);
            this.label26.TabIndex = 21;
            this.label26.Text = "Average Speed";
            // 
            // workoutSessionGraphTotalDistanceCovered
            // 
            this.workoutSessionGraphTotalDistanceCovered.AutoSize = true;
            this.workoutSessionGraphTotalDistanceCovered.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphTotalDistanceCovered.Location = new System.Drawing.Point(699, 328);
            this.workoutSessionGraphTotalDistanceCovered.Name = "workoutSessionGraphTotalDistanceCovered";
            this.workoutSessionGraphTotalDistanceCovered.Size = new System.Drawing.Size(46, 17);
            this.workoutSessionGraphTotalDistanceCovered.TabIndex = 20;
            this.workoutSessionGraphTotalDistanceCovered.Text = "300km";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(645, 298);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(153, 19);
            this.label28.TabIndex = 19;
            this.label28.Text = "Total Distance Covered";
            // 
            // workoutSessionGraphRangeLabel
            // 
            this.workoutSessionGraphRangeLabel.Location = new System.Drawing.Point(312, 397);
            this.workoutSessionGraphRangeLabel.Name = "workoutSessionGraphRangeLabel";
            this.workoutSessionGraphRangeLabel.Size = new System.Drawing.Size(110, 28);
            this.workoutSessionGraphRangeLabel.TabIndex = 13;
            this.workoutSessionGraphRangeLabel.Text = "Range: 200-300";
            this.workoutSessionGraphRangeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // workoutSessionUpdateShowOriginalData
            // 
            this.workoutSessionUpdateShowOriginalData.Location = new System.Drawing.Point(514, 399);
            this.workoutSessionUpdateShowOriginalData.Name = "workoutSessionUpdateShowOriginalData";
            this.workoutSessionUpdateShowOriginalData.Size = new System.Drawing.Size(109, 23);
            this.workoutSessionUpdateShowOriginalData.TabIndex = 12;
            this.workoutSessionUpdateShowOriginalData.Text = "Reset To Original";
            this.workoutSessionUpdateShowOriginalData.UseVisualStyleBackColor = true;
            this.workoutSessionUpdateShowOriginalData.Click += new System.EventHandler(this.workoutSessionUpdateShowOriginalData_Click);
            // 
            // workoutSessionUpdateDataRangeButton
            // 
            this.workoutSessionUpdateDataRangeButton.Location = new System.Drawing.Point(428, 399);
            this.workoutSessionUpdateDataRangeButton.Name = "workoutSessionUpdateDataRangeButton";
            this.workoutSessionUpdateDataRangeButton.Size = new System.Drawing.Size(75, 23);
            this.workoutSessionUpdateDataRangeButton.TabIndex = 11;
            this.workoutSessionUpdateDataRangeButton.Text = "Update";
            this.workoutSessionUpdateDataRangeButton.UseVisualStyleBackColor = true;
            this.workoutSessionUpdateDataRangeButton.Click += new System.EventHandler(this.workoutSessionUpdateDataRangeButton_Click);
            // 
            // workoutSessionTrackBarFinish
            // 
            this.workoutSessionTrackBarFinish.LargeChange = 1;
            this.workoutSessionTrackBarFinish.Location = new System.Drawing.Point(162, 400);
            this.workoutSessionTrackBarFinish.Name = "workoutSessionTrackBarFinish";
            this.workoutSessionTrackBarFinish.Size = new System.Drawing.Size(144, 45);
            this.workoutSessionTrackBarFinish.TabIndex = 10;
            this.workoutSessionTrackBarFinish.TickStyle = System.Windows.Forms.TickStyle.None;
            this.workoutSessionTrackBarFinish.ValueChanged += new System.EventHandler(this.workoutSessionTrackBarFinish_ValueChanged);
            // 
            // workoutSessionTrackBarStart
            // 
            this.workoutSessionTrackBarStart.LargeChange = 1;
            this.workoutSessionTrackBarStart.Location = new System.Drawing.Point(8, 399);
            this.workoutSessionTrackBarStart.Maximum = 4000;
            this.workoutSessionTrackBarStart.Name = "workoutSessionTrackBarStart";
            this.workoutSessionTrackBarStart.Size = new System.Drawing.Size(148, 45);
            this.workoutSessionTrackBarStart.TabIndex = 9;
            this.workoutSessionTrackBarStart.TickStyle = System.Windows.Forms.TickStyle.None;
            this.workoutSessionTrackBarStart.ValueChanged += new System.EventHandler(this.workoutSessionTrackBarStart_ValueChanged);
            // 
            // workoutSessionGraphNP
            // 
            this.workoutSessionGraphNP.AutoSize = true;
            this.workoutSessionGraphNP.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionGraphNP.Location = new System.Drawing.Point(891, 392);
            this.workoutSessionGraphNP.Name = "workoutSessionGraphNP";
            this.workoutSessionGraphNP.Size = new System.Drawing.Size(15, 17);
            this.workoutSessionGraphNP.TabIndex = 8;
            this.workoutSessionGraphNP.Text = "5";
            // 
            // workoutSessionGraphIF
            // 
            this.workoutSessionGraphIF.AutoSize = true;
            this.workoutSessionGraphIF.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionGraphIF.Location = new System.Drawing.Point(717, 391);
            this.workoutSessionGraphIF.Name = "workoutSessionGraphIF";
            this.workoutSessionGraphIF.Size = new System.Drawing.Size(15, 17);
            this.workoutSessionGraphIF.TabIndex = 7;
            this.workoutSessionGraphIF.Text = "8";
            // 
            // workoutSessionGraphTSS
            // 
            this.workoutSessionGraphTSS.AutoSize = true;
            this.workoutSessionGraphTSS.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionGraphTSS.Location = new System.Drawing.Point(877, 328);
            this.workoutSessionGraphTSS.Name = "workoutSessionGraphTSS";
            this.workoutSessionGraphTSS.Size = new System.Drawing.Size(29, 17);
            this.workoutSessionGraphTSS.TabIndex = 6;
            this.workoutSessionGraphTSS.Text = "315";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(836, 358);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 19);
            this.label10.TabIndex = 5;
            this.label10.Text = "Normalised Power";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(668, 358);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 19);
            this.label8.TabIndex = 4;
            this.label8.Text = "Intensity Factor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(827, 298);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Training Stress Score";
            // 
            // workoutSessionGraphFilter
            // 
            this.workoutSessionGraphFilter.CheckOnClick = true;
            this.workoutSessionGraphFilter.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionGraphFilter.FormattingEnabled = true;
            this.workoutSessionGraphFilter.Items.AddRange(new object[] {
            "Heart Rate",
            "Speed",
            "Cadence",
            "Altitude",
            "Power"});
            this.workoutSessionGraphFilter.Location = new System.Drawing.Point(8, 350);
            this.workoutSessionGraphFilter.MultiColumn = true;
            this.workoutSessionGraphFilter.Name = "workoutSessionGraphFilter";
            this.workoutSessionGraphFilter.Size = new System.Drawing.Size(613, 26);
            this.workoutSessionGraphFilter.TabIndex = 2;
            this.workoutSessionGraphFilter.SelectedIndexChanged += new System.EventHandler(this.workoutSessionGraphFilter_SelectedIndexChanged);
            // 
            // workoutSessionGraph
            // 
            this.workoutSessionGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.workoutSessionGraph.IsShowPointValues = false;
            this.workoutSessionGraph.Location = new System.Drawing.Point(8, 14);
            this.workoutSessionGraph.Name = "workoutSessionGraph";
            this.workoutSessionGraph.PointValueFormat = "G";
            this.workoutSessionGraph.Size = new System.Drawing.Size(613, 337);
            this.workoutSessionGraph.TabIndex = 0;
            // 
            // workoutSessionsPanel
            // 
            this.workoutSessionsPanel.Controls.Add(this.workoutSessionsGridView);
            this.workoutSessionsPanel.Location = new System.Drawing.Point(4, 6);
            this.workoutSessionsPanel.Name = "workoutSessionsPanel";
            this.workoutSessionsPanel.Size = new System.Drawing.Size(1041, 539);
            this.workoutSessionsPanel.TabIndex = 1;
            // 
            // workoutSessionsGridView
            // 
            this.workoutSessionsGridView.AllowUserToAddRows = false;
            this.workoutSessionsGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.Padding = new System.Windows.Forms.Padding(5);
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.workoutSessionsGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.workoutSessionsGridView.ColumnHeadersHeight = 60;
            this.workoutSessionsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.workoutSessionsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WorkoutID,
            this.WorkoutTime,
            this.WorkoutDate,
            this.WorkoutInterval,
            this.WorkoutLinkButton});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.workoutSessionsGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.workoutSessionsGridView.Location = new System.Drawing.Point(26, 25);
            this.workoutSessionsGridView.Name = "workoutSessionsGridView";
            this.workoutSessionsGridView.ReadOnly = true;
            this.workoutSessionsGridView.RowHeadersVisible = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.Padding = new System.Windows.Forms.Padding(10);
            this.workoutSessionsGridView.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.workoutSessionsGridView.RowTemplate.Height = 50;
            this.workoutSessionsGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.workoutSessionsGridView.Size = new System.Drawing.Size(996, 489);
            this.workoutSessionsGridView.TabIndex = 0;
            this.workoutSessionsGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.workoutSessionsGridView_CellContentClick);
            // 
            // workoutSessionsCalendar
            // 
            this.workoutSessionsCalendar.AllowItemEdit = false;
            this.workoutSessionsCalendar.AllowItemResize = false;
            this.workoutSessionsCalendar.AllowNew = false;
            this.workoutSessionsCalendar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            calendarHighlightRange11.DayOfWeek = System.DayOfWeek.Monday;
            calendarHighlightRange11.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange11.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange12.DayOfWeek = System.DayOfWeek.Tuesday;
            calendarHighlightRange12.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange12.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange13.DayOfWeek = System.DayOfWeek.Wednesday;
            calendarHighlightRange13.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange13.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange14.DayOfWeek = System.DayOfWeek.Thursday;
            calendarHighlightRange14.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange14.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange15.DayOfWeek = System.DayOfWeek.Friday;
            calendarHighlightRange15.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange15.StartTime = System.TimeSpan.Parse("08:00:00");
            this.workoutSessionsCalendar.HighlightRanges = new System.Windows.Forms.Calendar.CalendarHighlightRange[] {
        calendarHighlightRange11,
        calendarHighlightRange12,
        calendarHighlightRange13,
        calendarHighlightRange14,
        calendarHighlightRange15};
            this.workoutSessionsCalendar.Location = new System.Drawing.Point(262, 34);
            this.workoutSessionsCalendar.MaximumFullDays = 1;
            this.workoutSessionsCalendar.MaximumViewDays = 70000;
            this.workoutSessionsCalendar.Name = "workoutSessionsCalendar";
            this.workoutSessionsCalendar.Size = new System.Drawing.Size(750, 480);
            this.workoutSessionsCalendar.TabIndex = 0;
            this.workoutSessionsCalendar.Text = "workoutSessionsCalendar";
            this.workoutSessionsCalendar.ItemClick += new System.Windows.Forms.Calendar.Calendar.CalendarItemEventHandler(this.workoutSessionsCalendar_ItemClick);
            // 
            // workoutSessionsCalendarMonth
            // 
            this.workoutSessionsCalendarMonth.ArrowsColor = System.Drawing.SystemColors.Window;
            this.workoutSessionsCalendarMonth.ArrowsSelectedColor = System.Drawing.Color.Gold;
            this.workoutSessionsCalendarMonth.DayBackgroundColor = System.Drawing.Color.Empty;
            this.workoutSessionsCalendarMonth.DayGrayedText = System.Drawing.SystemColors.GrayText;
            this.workoutSessionsCalendarMonth.DaySelectedBackgroundColor = System.Drawing.SystemColors.Highlight;
            this.workoutSessionsCalendarMonth.DaySelectedColor = System.Drawing.SystemColors.WindowText;
            this.workoutSessionsCalendarMonth.DaySelectedTextColor = System.Drawing.SystemColors.HighlightText;
            this.workoutSessionsCalendarMonth.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workoutSessionsCalendarMonth.ItemPadding = new System.Windows.Forms.Padding(2);
            this.workoutSessionsCalendarMonth.Location = new System.Drawing.Point(5, 31);
            this.workoutSessionsCalendarMonth.MonthTitleColor = System.Drawing.SystemColors.ActiveCaption;
            this.workoutSessionsCalendarMonth.MonthTitleColorInactive = System.Drawing.SystemColors.InactiveCaption;
            this.workoutSessionsCalendarMonth.MonthTitleTextColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.workoutSessionsCalendarMonth.MonthTitleTextColorInactive = System.Drawing.SystemColors.InactiveCaptionText;
            this.workoutSessionsCalendarMonth.Name = "workoutSessionsCalendarMonth";
            this.workoutSessionsCalendarMonth.Size = new System.Drawing.Size(251, 492);
            this.workoutSessionsCalendarMonth.TabIndex = 1;
            this.workoutSessionsCalendarMonth.Text = "monthView1";
            this.workoutSessionsCalendarMonth.TodayBorderColor = System.Drawing.Color.Maroon;
            this.workoutSessionsCalendarMonth.SelectionChanged += new System.EventHandler(this.workoutSessionsCalendarMonth_SelectionChanged);
            // 
            // effg
            // 
            this.effg.AutoSize = true;
            this.effg.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.effg.Location = new System.Drawing.Point(779, 302);
            this.effg.Name = "effg";
            this.effg.Size = new System.Drawing.Size(140, 19);
            this.effg.TabIndex = 19;
            this.effg.Text = "Training Stress Score";
            // 
            // workoutSessionTSS
            // 
            this.workoutSessionTSS.AutoSize = true;
            this.workoutSessionTSS.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.workoutSessionTSS.Location = new System.Drawing.Point(817, 333);
            this.workoutSessionTSS.Name = "workoutSessionTSS";
            this.workoutSessionTSS.Size = new System.Drawing.Size(29, 17);
            this.workoutSessionTSS.TabIndex = 20;
            this.workoutSessionTSS.Text = "150";
            // 
            // workoutSessionNP
            // 
            this.workoutSessionNP.AutoSize = true;
            this.workoutSessionNP.Location = new System.Drawing.Point(640, 386);
            this.workoutSessionNP.Name = "workoutSessionNP";
            this.workoutSessionNP.Size = new System.Drawing.Size(53, 13);
            this.workoutSessionNP.TabIndex = 21;
            this.workoutSessionNP.Text = "270 watts";
            // 
            // workoutSessionIF
            // 
            this.workoutSessionIF.AutoSize = true;
            this.workoutSessionIF.Location = new System.Drawing.Point(810, 385);
            this.workoutSessionIF.Name = "workoutSessionIF";
            this.workoutSessionIF.Size = new System.Drawing.Size(41, 13);
            this.workoutSessionIF.TabIndex = 22;
            this.workoutSessionIF.Text = "label32";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(792, 360);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(79, 13);
            this.label32.TabIndex = 23;
            this.label32.Text = "Intensity Factor";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(625, 360);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(92, 13);
            this.label33.TabIndex = 24;
            this.label33.Text = "Normalized Power";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(914, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 37;
            this.label11.Text = "Minimum Heart Rate";
            // 
            // workoutSessionGraphMinHeartRate
            // 
            this.workoutSessionGraphMinHeartRate.AutoSize = true;
            this.workoutSessionGraphMinHeartRate.Location = new System.Drawing.Point(947, 127);
            this.workoutSessionGraphMinHeartRate.Name = "workoutSessionGraphMinHeartRate";
            this.workoutSessionGraphMinHeartRate.Size = new System.Drawing.Size(41, 13);
            this.workoutSessionGraphMinHeartRate.TabIndex = 38;
            this.workoutSessionGraphMinHeartRate.Text = "label13";
            // 
            // workoutSessionMinHeartRate
            // 
            this.workoutSessionMinHeartRate.AutoSize = true;
            this.workoutSessionMinHeartRate.Location = new System.Drawing.Point(950, 126);
            this.workoutSessionMinHeartRate.Name = "workoutSessionMinHeartRate";
            this.workoutSessionMinHeartRate.Size = new System.Drawing.Size(39, 13);
            this.workoutSessionMinHeartRate.TabIndex = 40;
            this.workoutSessionMinHeartRate.Text = "60bpm";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(917, 97);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 13);
            this.label15.TabIndex = 39;
            this.label15.Text = "Minimum Heart Rate";
            // 
            // importWorkoutButton
            // 
            this.importWorkoutButton.BackColor = System.Drawing.SystemColors.HotTrack;
            this.importWorkoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.importWorkoutButton.FlatAppearance.BorderSize = 0;
            this.importWorkoutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.importWorkoutButton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importWorkoutButton.ForeColor = System.Drawing.Color.Snow;
            this.importWorkoutButton.Location = new System.Drawing.Point(417, 33);
            this.importWorkoutButton.Margin = new System.Windows.Forms.Padding(0);
            this.importWorkoutButton.Name = "importWorkoutButton";
            this.importWorkoutButton.Size = new System.Drawing.Size(112, 42);
            this.importWorkoutButton.TabIndex = 11;
            this.importWorkoutButton.Text = "Import File";
            this.importWorkoutButton.UseVisualStyleBackColor = false;
            this.importWorkoutButton.Click += new System.EventHandler(this.importWorkoutButton_Click);
            // 
            // WorkoutID
            // 
            this.WorkoutID.HeaderText = "WorkoutID";
            this.WorkoutID.Name = "WorkoutID";
            this.WorkoutID.ReadOnly = true;
            this.WorkoutID.Visible = false;
            // 
            // WorkoutTime
            // 
            this.WorkoutTime.HeaderText = "Time";
            this.WorkoutTime.Name = "WorkoutTime";
            this.WorkoutTime.ReadOnly = true;
            this.WorkoutTime.Width = 300;
            // 
            // WorkoutDate
            // 
            this.WorkoutDate.HeaderText = "Date";
            this.WorkoutDate.Name = "WorkoutDate";
            this.WorkoutDate.ReadOnly = true;
            this.WorkoutDate.Width = 300;
            // 
            // WorkoutInterval
            // 
            this.WorkoutInterval.HeaderText = "Interval";
            this.WorkoutInterval.Name = "WorkoutInterval";
            this.WorkoutInterval.ReadOnly = true;
            // 
            // WorkoutLinkButton
            // 
            this.WorkoutLinkButton.HeaderText = "View Workout Stats";
            this.WorkoutLinkButton.Name = "WorkoutLinkButton";
            this.WorkoutLinkButton.ReadOnly = true;
            this.WorkoutLinkButton.Text = "";
            this.WorkoutLinkButton.Width = 280;
            // 
            // CycleSoftware
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 641);
            this.Controls.Add(this.mainContentPanel);
            this.Controls.Add(this.topNavPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CycleSoftware";
            this.Text = "Cycle Tracker";
            this.topNavPanel.ResumeLayout(false);
            this.topNavPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.mainContentPanel.ResumeLayout(false);
            this.workoutSessionsCalendarPanel.ResumeLayout(false);
            this.workoutSessionPanel.ResumeLayout(false);
            this.workoutSessionPanel.PerformLayout();
            this.workoutSessionTableViewPanel.ResumeLayout(false);
            this.workoutSessionTableViewPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workoutSessionTableGridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.workoutSessionGraphViewPanel.ResumeLayout(false);
            this.workoutSessionGraphViewPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workoutSessionTrackBarFinish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workoutSessionTrackBarStart)).EndInit();
            this.workoutSessionsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.workoutSessionsGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topNavPanel;
        private System.Windows.Forms.Panel mainContentPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel workoutSessionPanel;
        private System.Windows.Forms.Label workoutSessionDateAndTime;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tableViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphViewToolStripMenuItem;
        private System.Windows.Forms.Panel workoutSessionsPanel;
        private System.Windows.Forms.Panel workoutSessionsCalendarPanel;
        private System.Windows.Forms.ToolStripMenuItem intervalToolStripMenuItem;
        private System.Windows.Forms.Panel workoutSessionIntervalPanel;
        private System.Windows.Forms.Panel workoutSessionGraphViewPanel;
        private System.Windows.Forms.Panel workoutSessionTableViewPanel;
        private ZedGraph.ZedGraphControl workoutSessionGraph;
        private System.Windows.Forms.DataGridView workoutSessionsGridView;
        private System.Windows.Forms.OpenFileDialog openWorkoutDialog;
        private System.Windows.Forms.FolderBrowserDialog openWorkoutsDialog;
        private System.Windows.Forms.DataGridView workoutSessionTableGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power;
        private System.Windows.Forms.Label workoutSessionTotalDistanceCovered;
        private System.Windows.Forms.Label distanceLabel;
        private System.Windows.Forms.Label workoutSessionMaxAltitude;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label workoutSessionAverageAltitude;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label workoutSessionMaxPower;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label workoutSessionAveragePower;
        private System.Windows.Forms.Label workoutSessionMaxHeartRate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label workoutSessionAverageHeartRate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label workoutSessionMaximumSpeed;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label workoutSessionAverageSpeed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox workoutSessionGraphFilter;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label workoutSessionGraphNP;
        private System.Windows.Forms.Label workoutSessionGraphIF;
        private System.Windows.Forms.Label workoutSessionGraphTSS;
        private System.Windows.Forms.TrackBar workoutSessionTrackBarFinish;
        private System.Windows.Forms.TrackBar workoutSessionTrackBarStart;
        private System.Windows.Forms.Button workoutSessionUpdateShowOriginalData;
        private System.Windows.Forms.Button workoutSessionUpdateDataRangeButton;
        private System.Windows.Forms.Label workoutSessionLengthLabel;
        private System.Windows.Forms.Label workoutSessionIntervalLabel;
        private System.Windows.Forms.Label workoutSessionGraphRangeLabel;
        private System.Windows.Forms.Label workoutSessionGraphMaxAltitude;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label workoutSessionGraphAverageAltitude;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label workoutSessionGraphMaxPower;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label workoutSessionGraphAveragePower;
        private System.Windows.Forms.Label workoutSessionGraphMaxHeartRate;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label workoutSessionGraphAverageHeartRate;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label workoutSessionGraphMaximumSpeed;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label workoutSessionGraphAverageSpeed;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label workoutSessionGraphTotalDistanceCovered;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label FTPLabel;
        private System.Windows.Forms.ComboBox UnitsComboBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button workoutSwitchToCalendarButton;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button workoutSwitchToGridButton;
        private System.Windows.Forms.Button importWorkoutsButton;
        private System.Windows.Forms.Calendar.MonthView workoutSessionsCalendarMonth;
        private System.Windows.Forms.Calendar.Calendar workoutSessionsCalendar;
        private System.Windows.Forms.Label workoutSessionIF;
        private System.Windows.Forms.Label workoutSessionNP;
        private System.Windows.Forms.Label workoutSessionTSS;
        private System.Windows.Forms.Label effg;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label workoutSessionGraphMinHeartRate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label workoutSessionMinHeartRate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button importWorkoutButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkoutID;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkoutTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkoutDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkoutInterval;
        private System.Windows.Forms.DataGridViewButtonColumn WorkoutLinkButton;
    }
}

