﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ini;
using ZedGraph;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms.Calendar;

namespace CyclingSoftware
{
    public partial class CycleSoftware : Form
    {
        public int WorkoutIdCounter = 0;
        public List <WorkoutSession> WorkoutSessionsList = new List<WorkoutSession>();
        public WorkoutSession CurrentWorkoutSession;
        public string SettingsFileName = "app_settings.txt";
        public string FTP;
        public string Units = "miles";

        public CycleSoftware()
        {
            InitializeComponent();

            workoutSessionPanel.Visible = false;
            workoutSessionGraphViewPanel.Visible = false;
            workoutSessionsCalendarPanel.Visible = false;

            LoadSettings();
            LoadFiles();
            UpdateFunctionalThresholdPower();

            // check all graph filters
            for (int x = 0; x < workoutSessionGraphFilter.Items.Count; x++)
            {
                workoutSessionGraphFilter.SetItemChecked(x, true);
            }

            // create contributor dictionary for use in ComboBox components
            var dict = new Dictionary<string, string>();

            // populate dictionary with ContributorList
            dict.Add("miles", "Miles");
            dict.Add("kilometers", "Kilometres");

            // set the addNewBugContributor ComboBox variables
            UnitsComboBox.DataSource = new BindingSource(dict, null);
            UnitsComboBox.DisplayMember = "Value";
            UnitsComboBox.ValueMember = "Key";

            if (Units == "miles")
            {
                UnitsComboBox.SelectedIndex = 0;
            } else
            {
                UnitsComboBox.SelectedIndex = 1;
            }
            
            workoutSessionsCalendar.ViewStart = DateTime.Parse("20/02/2011");
            workoutSessionsCalendarMonth.ViewStart = DateTime.Parse("01/03/2011");
            workoutSessionsCalendar.ViewStart = DateTime.Now.Date;
            workoutSessionsCalendar.ViewEnd = DateTime.Now.Date;
        }

        private void LoadSettings()
        {
            IniFile appInfo = new IniFile(AppDomain.CurrentDomain.BaseDirectory + "/" + SettingsFileName);

            FTP = appInfo.IniReadValue("Settings", "UserFTP");
            Units = appInfo.IniReadValue("Settings", "Units");
        }

        private void LoadFiles()
        {
            bool readData = false;
            string IniSectionName = "Files";
            string line;

            try
            {
                using (StreamReader streamReader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/" + SettingsFileName))
                {
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if (readData == true && !line.Contains("["))
                        {
                            if (line.Length > 0) addWorkout(line.Trim());
                        }

                        if (line.Contains(IniSectionName))
                        {
                            readData = true;
                        } else if (!line.Contains(IniSectionName) && line.Contains("["))
                        {
                            readData = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        private void UpdateFunctionalThresholdPower ()
        {
            FTPLabel.Text = FTP;
        }

        private void UpdateUnits(string unit)
        {
            IniFile appInfo = new IniFile(AppDomain.CurrentDomain.BaseDirectory + "/" + SettingsFileName);

            appInfo.IniWriteValue("Settings", "Units", unit);

            Units = unit;
        }

        private void AddWorkoutFileToSettings(string FileName)
        {
            using (StreamWriter streamWriter = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "/" + SettingsFileName, true))
            {
                streamWriter.WriteLine(FileName);
            }
        }

        private void addWorkout(string fileName)
        {
            string line;
            WorkoutSession workout;
            bool readData = false;
            string IniSectionName = "HRData";
            string fileUrl = AppDomain.CurrentDomain.BaseDirectory + "/files/" + fileName; 
            IniFile headerInfo = new IniFile(fileUrl);

            workout = new WorkoutSession(WorkoutIdCounter, 
            headerInfo.IniReadValue("Params", "Date"), 
            headerInfo.IniReadValue("Params", "StartTime"),
            headerInfo.IniReadValue("Params", "Length"),
            headerInfo.IniReadValue("Params", "Interval"));

            WorkoutIdCounter++;

            try
            {
                using (StreamReader streamReader = new StreamReader(fileUrl))
                {
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if (readData == true && !line.Contains("["))
                        {
                            char[] delimiterChars = { '\t' };
                            string[] substrings = line.Split(delimiterChars);
                                
                            if (substrings.Length == 6)
                            {
                                WorkoutSessionDataItem workoutSessionDataItem = new WorkoutSessionDataItem(WorkoutIdCounter,
                                int.Parse(substrings.ElementAt(0)),
                                double.Parse(substrings.ElementAt(1)) / 10,
                                int.Parse(substrings.ElementAt(2)),
                                double.Parse(substrings.ElementAt(3)),
                                int.Parse(substrings.ElementAt(4)),
                                int.Parse(substrings.ElementAt(5)));
                                workout.addSessionDataItem(workoutSessionDataItem);
                            }
                            else if (substrings.Length == 5)
                            {
                                WorkoutSessionDataItem workoutSessionDataItem = new WorkoutSessionDataItem(WorkoutIdCounter,
                                int.Parse(substrings.ElementAt(0)),
                                double.Parse(substrings.ElementAt(1)) / 10,
                                int.Parse(substrings.ElementAt(2)),
                                double.Parse(substrings.ElementAt(3)),
                                int.Parse(substrings.ElementAt(4)),
                                0);
                                workout.addSessionDataItem(workoutSessionDataItem);
                            } else
                            {
                                WorkoutSessionDataItem workoutSessionDataItem = new WorkoutSessionDataItem(WorkoutIdCounter,
                                int.Parse(substrings.ElementAt(0)),
                                0,
                                0,
                                0,
                                0,
                                0);
                                workout.addSessionDataItem(workoutSessionDataItem);
                            }
                        }

                        if (line.Contains(IniSectionName))
                        {
                            readData = true;
                        }
                        else if (!line.Contains(IniSectionName) && line.Contains("["))
                        {
                            readData = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            WorkoutSessionsList.Add(workout);

            addWorkoutToGridView(workout);          
        }

        private void addWorkoutToGridView(WorkoutSession workout)
        {
            string Date = workout.GetDate();
            string Time = workout.GetTime();
            string abbr = "";
            DateTime WorkoutDate = new DateTime(int.Parse(Date.Substring(0, 4)),
           int.Parse(Date.Substring(4, 2)),
           int.Parse(Date.Substring(6, 2)),
           int.Parse(Time.Substring(0, 2)),
           int.Parse(Date.Substring(3, 2)),
           0);

            if (WorkoutDate.Day == 1 || WorkoutDate.Day == 21 || WorkoutDate.Day == 31)
            {
                abbr = "st";
            }
            else if (WorkoutDate.Day == 2 || WorkoutDate.Day == 22)
            {
                abbr = "nd";
            }
            else if (WorkoutDate.Day == 3 || WorkoutDate.Day == 23)
            {
                abbr = "rd";
            }
            else
            {
                abbr = "th";
            }

            workoutSessionsGridView.Rows.Add(workout.GetId(), WorkoutDate.ToString(" h:mm") + WorkoutDate.ToString("tt").ToLower(), WorkoutDate.ToString(" d") + abbr + WorkoutDate.ToString(" MMMM yyyy"), workout.GetInterval(), "View Workout Session");
        }

        private void CalculateWorkoutAverages(int dataStart, int dataFinish)
        {
            if (dataStart >= dataFinish)
            {
                return;
            }

            string SpeedUnits;
            string DistanceUnits;
            string PowerUnits = " watts";
            string BPMUnits = " bpm";

            if (Units == "miles")
            {
                SpeedUnits = " mph";
                DistanceUnits = " miles";
            }
            else
            {
                SpeedUnits = " kph";
                DistanceUnits = " km";
            }
            
            workoutSessionTotalDistanceCovered.Text = workoutSessionGraphTotalDistanceCovered.Text = CurrentWorkoutSession.CalculateSessionTotalDistance(Units, workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + DistanceUnits;
            workoutSessionMaxHeartRate.Text = workoutSessionGraphMaxHeartRate.Text = CurrentWorkoutSession.CalculateSessionMaximumHeartRate(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + BPMUnits;
            workoutSessionMinHeartRate.Text = workoutSessionGraphMinHeartRate.Text = CurrentWorkoutSession.CalculateSessionMinimumHeartRate(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + BPMUnits;
            workoutSessionAverageHeartRate.Text = workoutSessionGraphAverageHeartRate.Text = CurrentWorkoutSession.CalculateSessionAverageHeartRate(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + BPMUnits;
            workoutSessionMaximumSpeed.Text = workoutSessionGraphMaximumSpeed.Text = CurrentWorkoutSession.CalculateSessionMaximumSpeed(Units, workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + SpeedUnits;
            workoutSessionAverageSpeed.Text = workoutSessionGraphAverageSpeed.Text =  CurrentWorkoutSession.CalculateSessionAverageSpeed(Units, workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + SpeedUnits;
            workoutSessionMaxPower.Text = workoutSessionGraphMaxPower.Text = CurrentWorkoutSession.CalculateSessionMaximumPower(Units, workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + PowerUnits;
            workoutSessionAveragePower.Text = workoutSessionGraphAveragePower.Text = CurrentWorkoutSession.CalculateSessionAveragePower(Units, workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + PowerUnits;
            workoutSessionMaxAltitude.Text = workoutSessionGraphMaxAltitude.Text = CurrentWorkoutSession.CalculateSessionMaximumAltitude(Units, workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + DistanceUnits;
            workoutSessionAverageAltitude.Text = workoutSessionGraphAverageAltitude.Text = CurrentWorkoutSession.CalculateSessionAverageAltitude(Units, workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + DistanceUnits;
        }

        private void CalculateWorkoutSpecials()
        {
            string PowerUnits = "watts";

            workoutSessionNP.Text = workoutSessionGraphNP.Text = CurrentWorkoutSession.CalculateSessionNormalizedPower(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString() + PowerUnits;
            workoutSessionGraphTSS.Text = workoutSessionTSS.Text = CurrentWorkoutSession.CalculateSessionTrainingStressScore(double.Parse(FTP), Units, workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString();
            workoutSessionGraphIF.Text = workoutSessionIF.Text = CurrentWorkoutSession.CalculateSessionIntensityFactor(double.Parse(FTP), workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value).ToString();
        }

        private void UpdateWorkoutSessionPanel()
        {
            string Date = CurrentWorkoutSession.GetDate();
            string Time = CurrentWorkoutSession.GetTime();
            string abbr = "";
            DateTime WorkoutDate = new DateTime(int.Parse(Date.Substring(0, 4)), 
            int.Parse(Date.Substring(4, 2)), 
            int.Parse(Date.Substring(6, 2)),
            int.Parse(Time.Substring(0, 2)),
            int.Parse(Date.Substring(3, 2)), 
            0);

            if (WorkoutDate.Day == 1 || WorkoutDate.Day == 21 || WorkoutDate.Day == 31)
            {
                abbr = "st";
            } else if (WorkoutDate.Day == 2 || WorkoutDate.Day == 22)
            {
                abbr = "nd";
            } else if (WorkoutDate.Day == 3 || WorkoutDate.Day == 23)
            {
                abbr = "rd";
            } else
            {
                abbr = "th";
            }

            workoutSessionDateAndTime.Text = "Workout On:" + WorkoutDate.ToString(" d") + abbr + WorkoutDate.ToString(" MMMM yyyy") + " at" + WorkoutDate.ToString(" h:mm") + WorkoutDate.ToString("tt").ToLower();
            workoutSessionLengthLabel.Text = "Length: " + CurrentWorkoutSession.GetLength();
            workoutSessionIntervalLabel.Text = "Interval: " + CurrentWorkoutSession.GetInterval();

            workoutSessionTrackBarStart.Minimum = 1;
            workoutSessionTrackBarStart.Maximum = CurrentWorkoutSession.getSessionData().Count;
            workoutSessionTrackBarStart.Value = 1;
            workoutSessionTrackBarFinish.Minimum = 1;
            workoutSessionTrackBarFinish.Maximum = CurrentWorkoutSession.getSessionData().Count;
            workoutSessionTrackBarFinish.Value = CurrentWorkoutSession.getSessionData().Count;

            PopulateWorkoutSessionTableGridView();
            CalculateWorkoutAverages(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value);
            CalculateWorkoutSpecials();
            PopulateWorkoutSessionGraph(0, CurrentWorkoutSession.getSessionData().Count);
            PopulateWorkoutSessionIntervals();
            tableViewToolStripMenuItem.PerformClick();
        }

        private void PopulateWorkoutSessionIntervals()
        {
              
        }

        private void PopulateWorkoutSessionGraph(int dataStart, int dataFinish)
        {
            // check if dataStart is greater than dataFinish which will cause errors if true
            if (dataStart >= dataFinish)
            {
                MessageBox.Show("The minimum range needs to be higher than maximum range.", "Attention!");
                return;
            }

            GraphPane Graph = workoutSessionGraph.GraphPane;

            Graph.GraphItemList.Clear();
            Graph.CurveList.Clear();

            Graph.Title = "Workout Session Statistics Graph";
            Graph.XAxis.Title = "Time (Seconds)";
            Graph.YAxis.Title = "Value";

            PointPairList HeartRateGraphPointList = new PointPairList();
            PointPairList SpeedGraphPointList = new PointPairList();
            PointPairList CadenceGraphPointList = new PointPairList();
            PointPairList AltitudeGraphPointList = new PointPairList();
            PointPairList PowerGraphPointList = new PointPairList();
            
            for (int i = 0; i < CurrentWorkoutSession.getSessionData().Count; i++)
            {
                if ((i + 1) >= dataStart && (i + 1) <= dataFinish)
                {
                    HeartRateGraphPointList.Add(i * int.Parse(CurrentWorkoutSession.GetInterval()), CurrentWorkoutSession.getSessionData().ElementAt(i).GetHeartRate());
                    SpeedGraphPointList.Add(i * int.Parse(CurrentWorkoutSession.GetInterval()), CurrentWorkoutSession.getSessionData().ElementAt(i).GetSpeed());
                    CadenceGraphPointList.Add(i * int.Parse(CurrentWorkoutSession.GetInterval()), CurrentWorkoutSession.getSessionData().ElementAt(i).GetCadence());
                    AltitudeGraphPointList.Add(i * int.Parse(CurrentWorkoutSession.GetInterval()), CurrentWorkoutSession.getSessionData().ElementAt(i).GetAltitude());
                    PowerGraphPointList.Add(i * int.Parse(CurrentWorkoutSession.GetInterval()), CurrentWorkoutSession.getSessionData().ElementAt(i).GetPower());
                }
            }

            foreach (object itemChecked in workoutSessionGraphFilter.CheckedItems)
            {
                switch(itemChecked.ToString())
                {
                    case "Heart Rate":
                        LineItem HeartRateLineItem = Graph.AddCurve("Heart Rate (BPM)", HeartRateGraphPointList, Color.Red, SymbolType.None);               
                    break;
                    case "Speed":
                        LineItem SpeedLineItem = Graph.AddCurve("Speed (KPH)", SpeedGraphPointList, Color.Blue, SymbolType.None);
                    break;
                    case "Cadence":
                        LineItem CadenceLineItem = Graph.AddCurve("Cadence (RPM)", CadenceGraphPointList, Color.Green, SymbolType.None);
                    break;
                    case "Altitude":
                        LineItem AltitudeLineItem = Graph.AddCurve("Altitude (Meters)", AltitudeGraphPointList, Color.Orange, SymbolType.None);
                    break;
                    case "Power":
                        LineItem PowerLineItem = Graph.AddCurve("Power (Watts)", PowerGraphPointList, Color.SlateGray, SymbolType.None);
                    break;
                }
            }

            workoutSessionGraph.AxisChange();
            workoutSessionGraph.Invalidate();
        }

        private void PopulateWorkoutSessionTableGridView()
        {
            workoutSessionTableGridView.Rows.Clear();

            for (int i = 0; i < CurrentWorkoutSession.getSessionData().Count; i++)
            {
                workoutSessionTableGridView.Rows.Add(CurrentWorkoutSession.getSessionData().ElementAt(i).GetHeartRate(),
                CurrentWorkoutSession.getSessionData().ElementAt(i).GetSpeed(),
                CurrentWorkoutSession.getSessionData().ElementAt(i).GetCadence(),
                CurrentWorkoutSession.getSessionData().ElementAt(i).GetAltitude(),
                CurrentWorkoutSession.getSessionData().ElementAt(i).GetPower());
            }
        }

        private void workoutSessionsGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                for (int i = 0; i < WorkoutSessionsList.Count; i++)
                {
                    if (WorkoutSessionsList.ElementAt(i).GetId() == (int) workoutSessionsGridView.Rows[e.RowIndex].Cells[0].Value)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        CurrentWorkoutSession = WorkoutSessionsList.ElementAt(i);

                        UpdateWorkoutSessionPanel();

                        workoutSessionsPanel.Visible = false;
                        workoutSessionPanel.Visible = true;
                    }
                }
            }
        }

        private void tableViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            workoutSessionGraphViewPanel.Visible = false;
            workoutSessionTableViewPanel.Visible = true;
            workoutSessionIntervalPanel.Visible = false;
        }

        private void graphViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            workoutSessionGraphViewPanel.Visible = true;
            workoutSessionTableViewPanel.Visible = false;
            workoutSessionIntervalPanel.Visible = false;
        }

        private void workoutSessionGraphFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateWorkoutSessionGraph(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value);
        }

        private void workoutSessionTrackBarFinish_ValueChanged(object sender, EventArgs e)
        {
            workoutSessionGraphRangeLabel.Text = "Range: " + workoutSessionTrackBarStart.Value + " to " + workoutSessionTrackBarFinish.Value;
        }

        private void workoutSessionTrackBarStart_ValueChanged(object sender, EventArgs e)
        {
            workoutSessionGraphRangeLabel.Text = "Range: " + workoutSessionTrackBarStart.Value + " to " + workoutSessionTrackBarFinish.Value;
        }

        private void workoutSessionUpdateDataRangeButton_Click(object sender, EventArgs e)
        {
            PopulateWorkoutSessionGraph(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value);
            CalculateWorkoutAverages(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value);
        }

        private void workoutSessionUpdateShowOriginalData_Click(object sender, EventArgs e)
        {
            workoutSessionTrackBarStart.Value = 1;
            workoutSessionTrackBarFinish.Value = CurrentWorkoutSession.getSessionData().Count;

            PopulateWorkoutSessionGraph(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value);
        }

        private void calendar1_LoadItems(object sender, System.Windows.Forms.Calendar.CalendarLoadEventArgs e)
        {

        }

        private void UnitsComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateUnits(UnitsComboBox.SelectedValue.ToString());
            if (CurrentWorkoutSession != null) CalculateWorkoutAverages(workoutSessionTrackBarStart.Value, workoutSessionTrackBarFinish.Value);
        }

        private void importWorkoutsButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openWorkoutsDialog.ShowDialog();
            string FolderName = "";

            openWorkoutsDialog.ShowNewFolderButton = false;

            if (result == DialogResult.OK)
            {
                FolderName = openWorkoutsDialog.SelectedPath;
                DirectoryInfo d = new DirectoryInfo(FolderName);

                foreach (var file in d.GetFiles("*.hrm"))
                {
                    try {
                        var source = Path.Combine(FolderName, file.Name);
                        var destination = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/files/", file.Name);

                        File.Copy(source, destination);

                        AddWorkoutFileToSettings(file.Name);

                        addWorkout(file.Name);
                    } catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString());
                    }
                }
                workoutSwitchToGridButton.PerformClick();
            }
        }

        private void workoutSwitchToGridButton_Click(object sender, EventArgs e)
        {
            workoutSessionPanel.Visible = false;
            workoutSessionsCalendarPanel.Visible = false;
            workoutSessionsPanel.Visible = true;
        }

        private void workoutSwitchToCalendarButton_Click(object sender, EventArgs e)
        {
            workoutSessionPanel.Visible = false;
            workoutSessionsCalendarPanel.Visible = true;
            workoutSessionsPanel.Visible = false;
        }

        private void workoutSessionsCalendarMonth_SelectionChanged(object sender, EventArgs e)
        {
            string CurrentDate = workoutSessionsCalendarMonth.SelectionStart.ToString();
            char[] delimiterChars = { ' ' };
            string[] CalendarDateSplit = CurrentDate.Split(delimiterChars);

            // fixes a weird bug with calendar control
            if (workoutSessionsCalendar.ViewStart.Date < DateTime.Parse(CalendarDateSplit[0]))
            {
                workoutSessionsCalendar.ViewEnd = DateTime.Now;
                workoutSessionsCalendar.ViewStart = DateTime.Parse(CalendarDateSplit[0]);
                workoutSessionsCalendar.ViewEnd = DateTime.Parse(CalendarDateSplit[0] + " 23:59");
            } else
            {
                workoutSessionsCalendar.ViewEnd = DateTime.Now;
                workoutSessionsCalendar.ViewStart = DateTime.Parse(CalendarDateSplit[0]);
                workoutSessionsCalendar.ViewEnd = DateTime.Parse(CalendarDateSplit[0] + " 23:59");
            }


            for (int i = 0; i < WorkoutSessionsList.Count; i++)
            {
                string WorkoutDateTimeString = WorkoutSessionsList.ElementAt(i).GetDate();
                WorkoutDateTimeString = WorkoutDateTimeString.Substring(6, 2) + "/" + WorkoutDateTimeString.Substring(4, 2) + "/" + WorkoutDateTimeString.Substring(0, 4);

                if (WorkoutDateTimeString == CalendarDateSplit[0])
                {
                    CalendarItem name;
                    TimeSpan WorkoutLength = TimeSpan.Parse(WorkoutSessionsList.ElementAt(i).GetLength());
                    string WorkoutStartTime = WorkoutSessionsList.ElementAt(i).GetTime();
                    name = new CalendarItem(workoutSessionsCalendar, DateTime.Parse(WorkoutDateTimeString + " " + WorkoutStartTime), WorkoutLength, "Workout ID: " + WorkoutSessionsList.ElementAt(i).GetId().ToString() + " on " + WorkoutSessionsList.ElementAt(i).GetTime() + " for " + WorkoutLength.Minutes.ToString() + " minutes.");
                    workoutSessionsCalendar.Items.Add(name);
                }
            }
        }

        private void workoutSessionsCalendar_ItemClick(object sender, CalendarItemEventArgs e)
        {
            char[] delimiterChars = { ' ' };
            string[] substrings = e.Item.Text.Split(delimiterChars);

            for (int i = 0; i < WorkoutSessionsList.Count; i++)
            {
                if (int.Parse(substrings[2]) == WorkoutSessionsList.ElementAt(i).GetId())
                {
                    CurrentWorkoutSession = WorkoutSessionsList.ElementAt(i);

                    UpdateWorkoutSessionPanel();

                    workoutSessionsPanel.Visible = false;
                    workoutSessionsCalendarPanel.Visible = false;
                    workoutSessionPanel.Visible = true;
                }
            }
        }

        private void importWorkoutButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openWorkoutDialog.ShowDialog();
            openWorkoutDialog.Multiselect = false;
            string fileName = "";

            if (result == DialogResult.OK)
            {
                fileName = openWorkoutDialog.FileName;
                var destination = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/files/", openWorkoutDialog.SafeFileName);

                try {
                    File.Copy(fileName, destination);

                    AddWorkoutFileToSettings(openWorkoutDialog.SafeFileName);

                    addWorkout(openWorkoutDialog.SafeFileName);
                } catch (IOException ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }

                workoutSwitchToGridButton.PerformClick();
            }

        }

        private void intervalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            workoutSessionIntervalPanel.Visible = true;
            workoutSessionGraphViewPanel.Visible = false;
            workoutSessionTableViewPanel.Visible = false;
        }
    }
}
