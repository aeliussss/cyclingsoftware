﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyclingSoftware
{
    public class WorkoutSession
    {
        private int Id;
        private string Date;
        private string Time;
        private string Length;
        private string Interval;
        private List<WorkoutSessionDataItem> sessionData = new List<WorkoutSessionDataItem>();
        private double ToKilometersFromMeters = 0.001;
        private double ToMilesFromMeters = 0.000621371;
        private double ToMPHFromKPH = 0.621371;
        public WorkoutSession (int Id, string Date, string Time, string Length, string Interval)
        {
            this.Id = Id;
            this.Date = Date;
            this.Time = Time;
            this.Length = Length;
            this.Interval = Interval;
        }

        public int GetId()
        {
            return this.Id;
        }

        public void SetId(int Id)
        {
            this.Id = Id;
        }

        public string GetDate()
        {
            return this.Date;
        } 

        public void SetDate(string Date)
        {
            this.Date = Date;
        }

        public string GetTime()
        {
            return this.Time;
        }

        public void SetTime(string Time)
        {
            this.Time = Time;
        }

        public string GetLength()
        {
            return this.Length;
        }

        public void SetLength(string Length)
        {
            this.Length = Length;
        }

        public string GetInterval()
        {
            return this.Interval;
        }

        public void SetInterval(string Interval)
        {
            this.Interval = Interval;
        }

        public List<WorkoutSessionDataItem> getSessionData ()
        {
            return this.sessionData;
        }

        public void addSessionDataItem(WorkoutSessionDataItem data)
        {
            sessionData.Add(data);
        }

        public double CalculateSessionTotalDistance (string Units, int dataStart, int dataFinish)
        {
            double distance = 0;

            for (int i = 0; i < sessionData.Count; i++)
            {
                if (Units == "miles")
                {
                    distance += (sessionData.ElementAt(i).GetSpeed() * ToMPHFromKPH) * (double.Parse(GetInterval()) / 3600);
                } else
                {
                    distance += (sessionData.ElementAt(i).GetSpeed()) * (double.Parse(GetInterval()) / 3600);
                }
            }
           
            return Math.Round(distance, 2);
        }

        public double CalculateSessionAverageSpeed (string Units, int dataStart, int dataFinish)
        {
            double total = 0;
            double amount = 0;

            for (int i = 0; i < sessionData.Count; i++)
            {
                if (Units == "miles")
                {
                    if ((i + 1) >= dataStart && (i + 1) <= dataFinish)
                    {
                        total += sessionData.ElementAt(i).GetSpeed() * ToMPHFromKPH;
                        amount++;
                    }
                }
                else
                {
                    if ((i + 1) >= dataStart && (i + 1) <= dataFinish)
                    {
                        total += sessionData.ElementAt(i).GetSpeed();
                        amount++;
                    }
                }
            }

            return Math.Round(total / amount,2);
        }

        public double CalculateSessionMaximumSpeed(string Units, int dataStart, int dataFinish)
        {
            double max = 0;

            if (Units == "miles")
            {
                for (int i = 0; i < sessionData.Count; i++)
                {
                    if (sessionData.ElementAt(i).GetSpeed() * ToMPHFromKPH > max && (i + 1) >= dataStart && (i + 1) <= dataFinish)
                    {
                        max = sessionData.ElementAt(i).GetSpeed() * ToMPHFromKPH;
                    }
                }
            }
            else
            {
                for (int i = 0; i < sessionData.Count; i++)
                {
                    if (sessionData.ElementAt(i).GetSpeed() > max && (i + 1) >= dataStart && (i + 1) <= dataFinish)
                    {
                        max = sessionData.ElementAt(i).GetSpeed();
                    }
                }
            }

            return Math.Round(max, 2);
        }

        public double CalculateSessionAverageHeartRate(int dataStart, int dataFinish)
        {
            int total = 0;
            double amount = 0;

            for (int i = 0; i < sessionData.Count; i++)
            {
                if ((i + 1) >= dataStart && (i + 1) <= dataFinish)
                {
                    total += sessionData.ElementAt(i).GetHeartRate();
                    amount++;
                }
            }

            return Math.Round(total / amount, 2);
        }

        public int CalculateSessionMaximumHeartRate(int dataStart, int dataFinish)
        {
            int max = 0;

            for (int i = 0; i < sessionData.Count; i++)
            {
                if (sessionData.ElementAt(i).GetHeartRate() > max && (i + 1) >= dataStart && (i + 1) <= dataFinish)
                {
                    max = sessionData.ElementAt(i).GetHeartRate();
                }
            }

            return max;
        }

        public int CalculateSessionMinimumHeartRate(int dataStart, int dataFinish)
        {
            int min = 99999;

            for (int i = 0; i < sessionData.Count; i++)
            {
                if (sessionData.ElementAt(i).GetHeartRate() < min && (i + 1) >= dataStart && (i + 1) <= dataFinish)
                {
                    min = sessionData.ElementAt(i).GetHeartRate();
                }
            }

            return min;
        }

        public double CalculateSessionAveragePower(string Units, int dataStart, int dataFinish)
        {
            double total = 0;
            int amount = 0;

            for (int i = 0; i < sessionData.Count; i++)
            {
                if ((i + 1) >= dataStart && (i + 1) <= dataFinish)
                {
                    total += sessionData.ElementAt(i).GetPower();
                    amount++;
                }
            }

            return Math.Round(total / amount, 2);
        }

        public int CalculateSessionMaximumPower(string Units, int dataStart, int dataFinish)
        {
            int max = 0;

            for (int i = 0; i < sessionData.Count; i++)
            {
                if (sessionData.ElementAt(i).GetPower() > max && (i + 1) >= dataStart && (i + 1) <= dataFinish)
                {
                    max = sessionData.ElementAt(i).GetPower();
                }
            }

            return max;
        }

        public double CalculateSessionAverageAltitude(string Units, int dataStart, int dataFinish)
        {
            double total = 0;
            int amount = 0;
            
            for (int i = 0; i < sessionData.Count; i++)
            {
                if (Units == "miles")
                {
                    if ((i + 1) >= dataStart && (i + 1) <= dataFinish)
                    {
                        total += sessionData.ElementAt(i).GetAltitude() * ToMilesFromMeters;
                        amount++;
                    }
                } else
                {
                    if ((i + 1) >= dataStart && (i + 1) <= dataFinish)
                    {
                        total += sessionData.ElementAt(i).GetAltitude() * ToKilometersFromMeters;
                        amount++;
                    }
                }
            }

            return Math.Round(total / amount,2);
        }

        public double CalculateSessionMaximumAltitude(string Units, int dataStart, int dataFinish)
        {
            double max = -99999;

            if (Units == "miles")
            {
                for (int i = 0; i < sessionData.Count; i++)
                {
                    if (sessionData.ElementAt(i).GetAltitude() * ToMilesFromMeters > max && (i + 1) >= dataStart && (i + 1) <= dataFinish)
                    {
                        max = sessionData.ElementAt(i).GetAltitude() * ToMilesFromMeters;
                    }
                }
            }
            else
            {
                for (int i = 0; i < sessionData.Count; i++)
                {
                    if (sessionData.ElementAt(i).GetAltitude() * ToKilometersFromMeters > max && (i + 1) >= dataStart && (i + 1) <= dataFinish)
                    {
                        max = sessionData.ElementAt(i).GetAltitude() * ToKilometersFromMeters;
                    }
                }
            }

            return Math.Round(max,2);
        }

        public double CalculateSessionNormalizedPower(int dataStart, int dataFinish)
        {
            double ResultingAveragesTotal = 0;
            double ResultingAveragesCount = 0;
            int interval = int.Parse(GetInterval());

            for (int i = 0; i < getSessionData().Count; i++)
            {
                double avg = 0;
                double total = 0;
                int rollingLength = 30 / interval;

                // start recording averages after 30 seconds
                if (interval * (i + 1) >= 30)
                {
                    for (int z = 0; z < rollingLength; z++)
                    {
                        total += Math.Pow(double.Parse(getSessionData().ElementAt(i - z).GetPower().ToString()), 4);
                    }

                    avg = total / rollingLength;
                    ResultingAveragesCount++;
                }

                ResultingAveragesTotal += avg;
            }

            return Math.Round(Math.Pow(ResultingAveragesTotal / ResultingAveragesCount, (1.0 / 4.0)));
        }

        public double CalculateSessionTrainingStressScore(double FunctionalThresholdPower, string Units, int dataStart, int dataFinish)
        {
            //TSS = (sec x NP® x IF®)/ (FTP x 3600) x 100
            int interval = int.Parse(GetInterval()) * getSessionData().Count;

            return Math.Round((interval * CalculateSessionNormalizedPower(dataStart, dataFinish) * CalculateSessionIntensityFactor(FunctionalThresholdPower, dataStart, dataFinish)) / (FunctionalThresholdPower * 3600) * 100, 0);
        }

        public double CalculateSessionIntensityFactor(double FunctionalThresholdPower, int dataStart, int dataFinish)
        {
            // IF® values are calculated by taking your Normalized Power® (NP) and dividing it by your Functional Threshold Power (FTP).
            return Math.Round(CalculateSessionNormalizedPower(dataStart, dataFinish) / FunctionalThresholdPower, 2);
        }
    }
}
