﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyclingSoftware
{
    public class WorkoutSessionDataItem
    {
        private int WorkoutSessionId;
        private int HeartRate;
        private double Speed;
        private int Cadence;
        private double Altitude;
        private int Power;
        private int PowerBalance;

        public WorkoutSessionDataItem(int WorkoutSessionId, int HeartRate, double Speed, int Cadence, double Altitude, int Power, int PowerBalance)
        {
            this.WorkoutSessionId = WorkoutSessionId;
            this.HeartRate = HeartRate;
            this.Speed = Speed;
            this.Cadence = Cadence;
            this.Altitude = Altitude;
            this.Power = Power;
            this.PowerBalance = PowerBalance;
        }

        public int GetWorkoutSessionId()
        {
            return this.WorkoutSessionId;
        }

        public int GetHeartRate()
        {
            return this.HeartRate;
        }

        public double GetSpeed()
        {
            return this.Speed;
        }

        public int GetCadence()
        {
            return this.Cadence;
        }

        public double GetAltitude()
        {
            return this.Altitude;
        }

        public int GetPower()
        {
            return this.Power;
        }

        public int GetPowerBalance()
        {
            return this.PowerBalance;
        }
    }
}
